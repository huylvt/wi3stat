#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <glib.h>
#include <time.h>
#include <unistd.h>

#include "modules/time.h"
#include "modules/mem.h"
#include "modules/net.h"
#include "modules/disk.h"
#include "modules/cpu.h"

#define COPYRIGHT "(C) 2014 huylvt <huylvt.vn@gmail.com>"
#define VERSION "0.1"
#define FILE_CONFIG "/.config/wi3stat/wi3stat.conf"
#define MAX_LINE 512

char *file_config;
short interval = 5;
char status_line[MAX_LINE];

void
help() {
    fprintf(stdout, "wi3stat %s - %s\n", VERSION, COPYRIGHT);
    fprintf(stdout, "USAGE: wi3stat [OPTIONS]\n");
    fprintf(stdout, "OPTIONS:\n");
    fprintf(stdout, "\t-c FILE\tuse FILE as config file. Default: %s\n", file_config);
    fprintf(stdout, "\t-h display this usage information.\n");
}


void
parse_cmd(int argc, char *argv[]) {
    char c;
    while ((c = getopt(argc, argv, "c:h")) != -1) {
        switch(c) {
            case 'h':
                help();
                exit(EXIT_SUCCESS);
                break;
            case 'c':
                free(file_config);
                file_config = optarg;
                break;
            default:
                fprintf(stderr, "Invaild arguments. Try -h for help.");
                exit(EXIT_FAILURE);
        }
    }
}


int 
main(int argc, char *argv[]) {
    char *home = getenv("HOME");
    file_config = malloc(strlen(home) + strlen(FILE_CONFIG) + 1);
    file_config = strcpy(file_config, home);
    file_config = strcat(file_config, FILE_CONFIG);
    parse_cmd(argc, argv);
    char *p;
    GKeyFile *config;
    gsize length, i;
    config = g_key_file_new();
    GError *error = NULL;
    if (!g_key_file_load_from_file(config, file_config, G_KEY_FILE_NONE, &error)) {
        fprintf(stderr, "Could not read config file %s\n", file_config);
        exit(EXIT_FAILURE);
    }
    gchar **modules = g_key_file_get_groups(config, &length);
    if (g_key_file_has_group(config, "general")) {
        if (g_key_file_has_key(config, "general", "interval", &error))
            interval = (short) g_key_file_get_integer(config, "general", "interval", &error);
    }
    fprintf(stdout, "{\"version\":1}\n[\n[],\n");

    while (1) {
        p = status_line;
        

        *(p++) = '[';
        for ( i = 0; i < length; i++ ) {
            char *module = modules[i];
            char now[30];
            char color[8];
            if (!strcmp(module, "general"))
                continue;
            
            if (!g_key_file_has_key(config, module, "type", &error)) {
                fprintf(stderr, "config error: module '%s' missing 'type' field.\n", module);
                exit(EXIT_FAILURE);
            }

            char *type = g_key_file_get_string(config, module, "type", &error);
            char *prefix = "";
            if (g_key_file_has_key(config, module, "prefix", &error))
                prefix = g_key_file_get_string(config, module, "prefix", &error);
            
            if (!strcmp(module, "disk")) {
                char *path = g_key_file_get_string(config, module, "path", &error);
                w_disk_info(now, color, path, type);
                p += sprintf(p, "{\"full_text\":\"%s %s %s\",\"color\":\"#ffffff\"},", prefix, path, now);
            }

            if (!strcmp(module, "memory")) {
                w_memory(now, color, type);
                p += sprintf(p, "{\"full_text\":\"%s %s\",\"color\":\"#ffffff\"},", prefix, now);
            }

            if (!strcmp(module, "network")) {
                w_net_info(now, color, type);
                p += sprintf(p, "{\"full_text\":\"%s %s\",\"color\":\"#ffffff\"},", prefix, now);
            }
            
            if (!strcmp(module, "cpu")) {
                w_cpu_load(now, color, type);
                p += sprintf(p, "{\"full_text\":\"%s %s\",\"color\":\"#ffffff\"},", prefix, now);
            }

            if (!strcmp(module, "time")) {
                char *format = g_key_file_get_string(config, module, "format", &error);
                w_time_now(now, format);
                p += sprintf(p, "{\"full_text\":\"%s\",\"color\":\"#ffffff\"},\n", now);
            }
        }
        *(p-2) = ']';
        *(p-1) = ',';
        *(p++) = '\0';
        fprintf(stdout, "%s\n", status_line);
        fflush(stdout);
        sleep(interval);
    }
    return 0;
}
