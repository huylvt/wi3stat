CC = gcc
CFLAGS = -Wall

GLIB_CFLAGS = $(shell pkg-config --cflags --libs glib-2.0)

TARGET = wi3stat

OBJS = obj/time.o obj/mem.o obj/disk.o obj/cpu.o obj/net.o

all: $(TARGET)

$(TARGET): $(OBJS) main.c
	$(CC) $(CFLAGS) -o $@ $^ $(GLIB_CFLAGS)

obj/%.o: modules/%.c modules/%.h
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) obj/*.o $(TARGET)
