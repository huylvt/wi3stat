#include <time.h>

void w_time_now(char out[30], char *format) {
	time_t now;
	struct tm* tm_info;
	time(&now);
    tm_info = localtime(&now);
    strftime(out, 30, format, tm_info);
}