#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void w_net_info (char out[30], char color[8], char *type) {
	struct ifaddrs *ifaddr, *ifa;
	int family, n;
	char host[NI_MAXHOST];
	char s[30];
	strcpy(out, "");
	if (getifaddrs(&ifaddr) == -1)
    	exit(EXIT_FAILURE);

    for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) {
    	
    	if (ifa->ifa_addr == NULL)
    		continue;

    	family = ifa->ifa_addr->sa_family;

    	if (family == AF_INET) {

    		if (getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), 
    					host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) != 0)
    			exit(EXIT_FAILURE);
    		if (strcmp(ifa->ifa_name, "lo")) {
    			sprintf(s, "%s %s, ", ifa->ifa_name, host);
    			strcat(out, s);
    		}
    	}
    }
    freeifaddrs(ifaddr);
    out[strlen(out) - 2] = '\0';
}