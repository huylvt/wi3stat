#include <stdio.h>
#include <stdlib.h>

void w_cpu_load(char out[30], char color[8], char *type) {
	float load_avg1, load_avg5, load_avg15;
	FILE *fp = fopen("/proc/loadavg", "r");
	if (fp == NULL) {
  		fprintf(stderr, "Can't open file!\n");
  		exit(EXIT_FAILURE);
	}
	if (fscanf(fp, "%f %f %f", &load_avg1, &load_avg5, &load_avg15) != 3) {
		sprintf(out, "??%%");
		fclose(fp);
		return;
	}
	fclose(fp);
	sprintf(out, "%.2f %.2f %.2f", load_avg1, load_avg5, load_avg15);
}