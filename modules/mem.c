#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define bufflen 20

void w_memory(char out[30], char color[8], char *type) {
	
	char name[bufflen];
	char unit[5];
	unsigned long mem_free, mem_total, mem_used, mem_buff, mem_cached, value;

    FILE *fp = fopen("/proc/meminfo", "r");
    if (fp == NULL) {
  		fprintf(stderr, "Can't open input file in.list!\n");
  		exit(EXIT_FAILURE);
	}
	while (fscanf(fp, "%s %lu %s", name, &value, unit) != EOF) {
		if (strcmp("MemTotal:", name) == 0)
			mem_total = value;
		else if (strcmp("MemFree:", name) == 0)
			mem_free = value;
		else if (strcmp("Buffers:", name) == 0)
			mem_buff = value;
		else if (strcmp("Cached:", name) == 0)
			mem_cached = value;
	}
	mem_used = mem_total - (mem_free + mem_buff + mem_cached);
	if (!strcmp(unit, "kB"))
		mem_used /= 1024;
	strcpy(unit, "MB");

	sprintf(out, "%lu %s", mem_used, unit);
}