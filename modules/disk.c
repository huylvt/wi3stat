#include <stdio.h>
#include <sys/statvfs.h>

#define TERABYTE (1024ULL * 1024 * 1024 * 1024)
#define GIGABYTE (1024ULL * 1024 * 1024)
#define MEGABYTE (1024ULL * 1024)
#define KILOBYTE (1024ULL)

void w_disk_info(char out[30], char color[8], char *path, char *type) {
	struct statvfs vfs_info;
	unsigned long long bytes;
	if (statvfs(path, &vfs_info) < 0) {
		sprintf(out, "??");
		return;
	}
	bytes = vfs_info.f_bsize*(unsigned long long)vfs_info.f_bavail;
	if (bytes > TERABYTE)
		sprintf(out, "%.02f TB", (double)bytes / TERABYTE);
	else if (bytes > GIGABYTE)
		sprintf(out, "%.01f GB", (double)bytes / GIGABYTE);
	else if (bytes > MEGABYTE)
		sprintf(out, "%.01f MB", (double)bytes / MEGABYTE);
	else if (bytes > KILOBYTE)
		sprintf(out, "%.01f KB", (double)bytes / KILOBYTE);
	else
		sprintf(out, "%.01f B", (double)bytes);
}